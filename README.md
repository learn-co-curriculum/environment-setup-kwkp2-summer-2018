## Working With Learn

Throughout this course, we will be using the Learn IDE to interact with and write our code. The quickest way to get started is to use the in-browser IDE. Please take a look at [this document][ide-documentation], which provides an overview of how to begin.

All lessons have a GitHub link provided, where they can be forked and cloned to your own personal repositories. If you would like to test/write any of the code locally, you will need to address any environment issues that come with it (i.e. having the right Node version, having all required dependencies such as `http-server`, etc).

For now, we recommend sticking with the in-browser IDE as it has removed many of the barriers to entry. While solving environment/dependency issues is major part of your average programmers life, don't hesitate to ask for help if you are encountering issues that seem outside of the immediate lesson's scope. 

[ide-documentation]: http://help.learn.co/the-learn-ide/ide-in-browser
